---
title: Reading List
layout: "page"
icon: fa-book
order: 3
---

Here are some readings. Enjoy them!

1. [Design Thinking is like syphilis](https://medium.com/@sts_news/design-thinking-is-kind-of-like-syphilis-its-contagious-and-rots-your-brains-842ed078af29)  
2. [Fab City Whitepaper](https://github.com/fabcity/fabcity.github.io/blob/master/whitepaper.pdf)
3. [Designing and Making for the Real World](https://blog.fab.city/fab-city-prototypes-designing-and-making-for-the-real-world-e97e9b04857)  
4. [Design as Participation](https://jods.mitpress.mit.edu/pub/design-as-participation)  
5. [Embodied Design Ideation Methods](https://www.dropbox.com/s/t5pxd3ae5wc91gj/WildeVallg%C3%A5rdaTomico_Embodied%20Design%20Ideation%20Methods-%20analysing%20the%20power%20of%20estrangement.pdf?dl=0)  
6. [An annotated portfolio on Doing Postphenomenology Through Research Products](https://www.dropbox.com/s/yn1ioyxx1ras77t/p459-hauser.pdf?dl=0)  
7. [Exemplary Design Research](https://www.dropbox.com/s/dsu7an9ygl5u1zv/exemplary.pdf?dl=0)  
8. [Deployments of the table-non-table](https://www.dropbox.com/s/ora6mp3q7mkrfwp/paper201-hauser-TNT-CHI2018.pdf?dl=0)  
9. [Prototypes and Prototyping Design Research](https://www.dropbox.com/s/yljt1r0n6hbu7rd/PrototypesChapterFinalDRAFT.pdf?dl=0)  
10. [The Tyranny of Convenience](https://www.nytimes.com/2018/02/16/opinion/sunday/tyranny-convenience.html)  
11. [Capitalism Killed Our Climate Momentum](https://theintercept.com/2018/08/03/climate-change-new-york-times-magazine/)  
12. [Designing your life](https://www.amazon.com/Designing-Your-Life-Well-Lived-Joyful/dp/1101875321/ref=sr_1_1?ie=UTF8&qid=1535288486&sr=8-1&keywords=designing+your+life)  
13. [An Aesthetics of Participation](https://www.guggenheim.org/blogs/lablog/an-aesthetics-of-participation)  
14. [Taking A Part](https://mitpress.mit.edu/books/taking-apart)  
15. [Participation](https://mitpress.mit.edu/books/participation) 
16. [Technological revolutions and techno-economic paradigms](http://technologygovernance.eu/files/main/2009070708552121.pdf)  
17. [Networked Intelligence](https://v3.pubpub.org/pub/networked-intelligence)  
18. [Extended Intelligence](https://pubpub.ito.com/pub/extended-intelligence)  
19. [2015 : WHAT DO YOU THINK ABOUT MACHINES THAT THINK?](https://www.edge.org/contributors/q2015)  
20. [Anatomy of an AI system](https://anatomyof.ai/)  
21. [We need a post-liberal order now](https://www.economist.com/open-future/2018/09/26/we-need-a-post-liberal-order-now)  

