---
title: MDEF Design Studio
layout: "page"
order: 1
---

Understand, develop and propose interventions in the real world.  

![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/MVIMG_20190111_104531_v2.jpg)

### Faculty
Tomas Diez  
Oscar Tomico

### Assistant
Mariana Quintero  

### Syllabus and Learning Objectives
MDEF Research, Design and Development studios aim to take research areas of interest and initial project ideas into an advanced concretion point, and execution plan. The studio structure in three terms could be understood as follows:
* TERM 1: Research Studio: Analyzing the past. References, state of the art. Identifying areas of interest.  
* TERM 2: Design Studio: Forming the present. Building the foundations. Applying knowledge into practice. Prototyping and experimenting. 
* TERM 3: Development Studio: Defining the future. Establishing roadmaps. Forming partnerships. Testing ideas and prototypes in the real world. 

MDEF’s Design Studio aims to evolve the work developed by students during the first term of the Master program (Research Studio). After identifying areas of interest, and proposing initial project ideas, students will be encouraged to develop further their projects into specific proposals, focusing on designing interventions in the real world. The studio will be supported seminars (or tracks), including the Fab Academy course, Material Driven Design, Reflection through making, and the Atlas of the Weak Signals (definition, and development). 

The Design Studio time will be dedicated to supporting students to focus their work on the development of their design intervention or project. During the studio, studio leaders will bring invited guests to introduce topics of interest to the process and to participate in tutorials during the desk crits.  

### Total Duration
Weekly studio, every Tuesday from 13hrs to 19hrs.  
13 - 15 pitch over lunch. Lunch brought by each student.  
15 - 19 desk crits, tutorials.  

### Structure and Phases
* *Week 1: Jan 22*     
13hrs - 15hrs. Pitch over lunch: Research trip presentations (China, Barcelona).   
15hrs - 16hrs. Introduction to the studio.  
16hrs - 19hrs. Desk crits of reflections after research trip, and how to incorporate to project development process.  
* *Week 2: Jan 29*    
13hrs - 15hrs pitch over lunch.  
15hrs - 19hrs desk crits.  
* *Week 3: Feb 5*    
Visit Design for City Making Exhibition at Elisava. Studio takes place at Elisava.  
15hrs - 16hrs Visit and introduction to DXCM  
16hrs - 19hrs DXCM workshop  
* *Week 4: Feb 12*    
13hrs - 15hrs pitch over lunch.  
15hrs - 19hrs desk crits.  
* *Week 5: Feb 19*    
Indy Johar visit to MDEF.  
Visit Tech Day about Barcelona local crafts and digital production at Elisava.    
13hrs - 15hrs pitches over lunch + Indy Johar presentation.    
15hrs - 15:30hrs Break.    
15:30hrs - 18hrs desk crits with Indy Johar & Jordi Closa.    
18:30hrs - 21hrs Visit Tech Day at Elisava.   
* *Week 6: Feb 26*    
Visit event on Social Robotics at Elisava. Studio takes place at Elisava.  
13hrs - 15hrs pitch over lunch.  
15hrs - 18:30hrs desk crits.  
18:30hrs - 21hrs visit Social Robotics event.  
* *Week 7: Mar 5*    
13hrs - 19hrs - MIDTERM REVIEWS.  
* *Week 8: Mar 12*    
13hrs - 15hrs pitch over lunch + Lecture by invited guest.  
15hrs - 19hrs desk crits.  
* *Week 9: Mar 19*    
13hrs - 15hrs pitch over lunch.  
15hrs - 19hrs desk crits.  
* *Week 10: Mar 26* 
Saúl Baeza visit to MDEF.   
13hrs - 15hrs pitch over lunch + Saúl Baeza presentation.   
15hrs - 19hrs desk crits.    
* *Week 11: Apr 2*    
13hrs - 15hrs pitch over lunch.  
15hrs - 19hrs desk crits.  
* *Week 12: Apr 9*    
13hrs - 19hrs - FINAL PRESENTATIONS.  

### Bibliography
[Speculative Everything	- Anthony Dunne and Fiona Raby](https://mitpress.mit.edu/books/speculative-everything)  
[Adversarial Design	- Carl DiSalvo](https://mitpress.mit.edu/books/adversarial-design)  
[Massive Change - Bruce Mau, Jennifer Leonard and Institute without Boundaries](http://www.brucemaudesign.com/work/massive-change)  
[Design for the Real World: Human Ecology and Social Change	- Victor Papanek](https://www.amazon.com/Design-%C2%ADReal-%C2%ADWorld-%C2%ADEcology-%C2%ADSocial/dp/0897331532)  
[Liquid Modernity - Zygmunt Bauman](https://www.amazon.com/Liquid-%C2%ADModernity-%C2%ADZygmunt-%C2%ADBauman/dp/0745624103)  
[Who Owns the Future? - Jason Lanier](https://www.amazon.es/gp/product/B008J2AEY8/ref=dp-%C2%ADkindle-%C2%ADredirect?ie=UTF8&btkr=1)  
[This Changes Everything - Naomi Klein](https://thischangeseverything.org/book/)  
[To Save Everything, Click Here: The Folly of Technological Solutionism - Evgeny Morozov](https://www.amazon.com/gp/product/1610393708?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s00)  
[Democratizing Innovation - Eric Von Hippel](https://www.amazon.com/gp/product/0262720477?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s01)  
[Cradle to Cradle: Remaking the Way We Make Things - Michael Braungart, William McDonough](http://www.amazon.com/gp/product/0865475873?psc=1&redirect=true&ref_=od_aui_detailpages00)  
[Macrowikinomics: New Solutions for a Connected Planet - Don Tapscott, Anthony D. Williams](http://www.amazon.com/gp/product/1591844282?psc=1&redirect=true&ref_=od_aui_detailpages00)  
[The Third Industrial Revolution: How Lateral Power Is Transforming Energy, the Economy, and the World - Jeremy Rifkin](http://www.amazon.com/gp/product/0230115217?psc=1&redirect=true&ref_=od_aui_detailpages00g)  
[The Death and Life of Great American Cities - Jane Jacobs](https://www.amazon.com/Death-Life-Great-American-Cities/dp/067974195X)    
[The Third Plate - Dan Barber](http://www.thethirdplate.com/)  
[Free Innovation - Eric Von Hippel](https://www.amazon.com/Free-Innovation-Press-Eric-Hippel/dp/0262035219)  
[Limits to Growths - Donella H. Meadows](https://www.amazon.com/Limits-Growth-Donella-H-Meadows/dp/193149858X)  
[The Human Face of Big Data - Rick Smolan](https://www.amazon.com/Human-Face-Big-Data/dp/1454908270/ref=sr_1_1?s=books&ie=UTF8&qid=1509138845&sr=1-1&keywords=the+human+face+of+big+data)   

### TOMAS DIEZ
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/0302393192f2-Screenshot_2018_09_10_at_12.37.01_20180912191624503.jpg)

### Email Address
<tomasdiez@iaac.net>

### Personal Website
[Tomas Diez](http://tomasdiez.com)

### Twitter Account
@tomasdiez

### Facebook Profile
tomasdiez77

### Professional BIO
Tomas Diez is a Venezuelan Urbanist specialized in digital fabrication and its implications on the future cities and society. He is the co-founder of Fab Lab Barcelona, leads the Fab City Research Laboratory, and is a founding partner of the Fab City Global Initiative. He is the director of the Master in Design for Emergent Futures at the Institute for Advanced Architecture of Catalonia (IAAC) in Barcelona, where he is faculty in urban design and digital fabrication. Tomas is co-founder of other initiatives such as Smart Citizen (open source tools for citizen engagement), Fab City (locally productive, globally connected cities), Fablabs.io (the listing of fab labs in the world), and StudioP52 (art and design space in Barcelona).  

### OSCAR TOMICO
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/6cf613d0f7fa-IMG_9099_Edit_S.jpg)

### Email Address
<otomico@elisava.net>

### Personal Website
[Oscar Tomico - Elisava](http://www.elisava.net/en/center/professorate/oscar-tomico-plasencia)

### Twitter Account
@otomico

### Professional BIO
Oscar Tomico holds an MSc degree in Industrial Engineering from Polytechnic University of Catalonia (Spain) and a PhD from the same institution, awarded in 2007 with Cum Laude. During his research into Innovation Processes in Product Design, he investigated subjective experience-gathering techniques based on constructivist psychology. After finishing his PhD he worked as a consultant for Telefonica R&D (Barcelona). Tomico joined Eindhoven University of Technology (TU/e) in 2007 as Assistant Professor. He has been a guest researcher and lecturer at AUT Creative technologies (New Zealand), at TaiwanTech (Taiwan), Swedish School of Textiles (Sweden), Institute of Advanced Architecture (Spain), University of Tsukuba, Aalto (Finland) to name a few. During his sabbatical in 2015 he worked as a consultant for the functional textiles department at EURECAT (Spain). He recently (2017) became the head of the Industrial Design Bachelor’s degree program at ELISAVA University School of Design and Engineering of Barcelona.